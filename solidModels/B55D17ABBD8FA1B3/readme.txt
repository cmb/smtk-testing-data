-- 3dvia.com   --

The zip file Assemタービンカバー最終.IGS.zip contains the following files :
- readme.txt
- Assemタービンカバー最終.IGS


-- Model information --

Model Name : Assemタービンカバー最終
Author : Falcn
Publisher : Falcn

You can view this model here :
http://www.3dvia.com/content/B55D17ABBD8FA1B3
More models about this author :
http://www.3dvia.com/Falcn


-- Attached license --

A license is attached to the Assemタービンカバー最終 model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
