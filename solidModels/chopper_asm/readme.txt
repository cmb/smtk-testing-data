-- 3dvia.com   --

The zip file chopper_asm.igs.zip contains the following files :
- readme.txt
- chopper_asm.igs


-- Model information --

Model Name : chopper_asm
Author : N1ghtman-kpi
Publisher : N1ghtman-kpi

You can view this model here :
http://www.3dvia.com/content/2B11942133051729
More models about this author :
http://www.3dvia.com/N1ghtman-kpi


-- Attached license --

A license is attached to the chopper_asm model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
