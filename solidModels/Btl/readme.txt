-- 3dvia.com   --

The zip file Btl. MG 900 ml.IGS.zip contains the following files :
- readme.txt
- Btl. MG 900 ml.IGS


-- Model information --

Model Name : Btl. MG 900 ml
Author : 
Publisher : Subagio1

You can view this model here :
http://www.3dvia.com/content/9495738A9CAE8092
More models about this author :
http://www.3dvia.com/Subagio1


-- Attached license --

A license is attached to the Btl. MG 900 ml model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
