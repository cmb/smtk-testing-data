-- 3dvia.com   --

The zip file Brake Disc.igs.zip contains the following files :
- readme.txt
- Brake Disc.igs


-- Model information --

Model Name : Brake Disc
Author : m.Woodhead
Publisher : woody123

You can view this model here :
http://www.3dvia.com/content/E7AD08DDEFC1D3E5
More models about this author :
http://www.3dvia.com/woody123


-- Attached license --

A license is attached to the Brake Disc model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
