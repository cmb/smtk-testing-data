-- 3dvia.com   --

The zip file 8.igs.zip contains the following files :
- readme.txt
- 8.igs


-- Model information --

Model Name : 8.igs
Author : himanshu6000
Publisher : himanshu6000

You can view this model here :
http://www.3dvia.com/content/E10943D7E9FBCDDF
More models about this author :
http://www.3dvia.com/himanshu6000


-- Attached license --

A license is attached to the 8.igs model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution License 2.5
Detailed license : http://creativecommons.org/licenses/by/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
